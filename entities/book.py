from sqlalchemy import Column, Integer, String

from config.entityManager import Base


class Book(Base):
    __tablename__ = 'books'
    id = Column(Integer, primary_key=True)
    title = Column(String)
    author = Column(String)

    def clone(self, src):
        self.id = src.id
        self.title = src.title
        self.author = src.author

    def __repr__(self):
        return "<Book(id='{}, 'title='{}', author='{}')>".format(self.id, self.title, self.author)
