from sqlalchemy import event

from config.entityManager import EntityManager
from config.entityManager import Base


class LoadBalancer:

    def __init__(self, em: EntityManager) -> None:
        self.em = em
        self.val = Base
        event.listen(self.val, 'before_insert', self.before_insert, propagate=True)
        event.listen(self.val, 'before_update', self.before_update, propagate=True)
        event.listen(self.val, 'before_delete', self.before_remove, propagate=True)


    def load_balance(self):
        self.em.active_session = self.em.sessions[self.em.index]()
        print('current session : {}'.format(self.em.active_session.bind.engine.url))
        self.em.index = self.em.index + 1
        if self.em.index >= len(self.em.sessions):
            self.em.index = 0

        return self.em.active_session

    def before_insert(self, mapper, connection, target):
        url = connection.engine.url
        if self.em.engines[0].url.database != url.database:
            return

        for i, engine in enumerate(self.em.engines):
            if self.em.states_are_current[i]:
                if (engine.execute("LOCK TABLE " + str(mapper.mapped_table))):
                    print("Lock for {}".format(engine.url))

        print('Event before insert: ', url)

        for i, engine in enumerate(self.em.engines):
            if engine.url != url:
                print('Execute insert for {}'.format(engine.url))
                s = self.em.sessions[i]()
                new_obj = mapper.class_()
                s.add(new_obj)

        for i, engine in enumerate(self.em.engines):
            s = self.em.sessions[i]()
            s.commit()
            s.close()
            # No UNLOCK in postgres, it unlocks automatically after transaction
            if (engine.execute(
                    "SELECT PG_CLASS.RELNAME, PG_LOCKS.MODE FROM PG_CLASS, PG_LOCKS WHERE PG_CLASS.OID = PG_LOCKS.RELATION AND PG_CLASS.RELNAMESPACE >= 2200;")):
                print("No lock for {}".format(engine.url))

    def before_update(self, mapper, connection, target):
        url = connection.engine.url
        if self.em.engines[0].url.database != url.database:
            return
        print('Event before update: ', url)

        for i, engine in enumerate(self.em.engines):
            if self.em.states_are_current[i]:
                if (engine.execute("LOCK TABLE " + str(mapper.mapped_table))):
                    print("Lock for {}".format(engine.url))

        for i, engine in enumerate(self.em.engines):
            if engine.url != url:
                print('Execute update for {}'.format(engine.url))
                s = self.em.sessions[i]()
                # new_obj = mapper.class_()
                # self.val.clone(new_obj, target)

        for i, engine in enumerate(self.em.engines):
            s = self.em.sessions[i]()
            s.commit()
            s.close()
            # No UNLOCK in postgres, it unlocks automatically after transaction
            if (engine.execute(
                    "SELECT PG_CLASS.RELNAME, PG_LOCKS.MODE FROM PG_CLASS, PG_LOCKS WHERE PG_CLASS.OID = PG_LOCKS.RELATION AND PG_CLASS.RELNAMESPACE >= 2200;")):
                print("No lock for {}".format(engine.url))

    def before_remove(self, mapper, connection, target):
        url = connection.engine.url
        if self.em.engines[0].url.database != url.database:
            return
        print('Event before remove: ', url)

        for i, engine in enumerate(self.em.engines):
            if self.em.states_are_current[i]:
                if (engine.execute("LOCK TABLE " + str(mapper.mapped_table))):
                    print("Lock for {}".format(engine.url))

        for i, engine in enumerate(self.em.engines):
            if engine.url != url:
                print('Execute remove for {}'.format(engine.url))
                s = self.em.sessions[i]()
                classname = mapper.class_
                print(target.id)
                s.query(classname).filter(classname.id == target.id).delete()

        for i, engine in enumerate(self.em.engines):
            s = self.em.sessions[i]()
            s.commit()
            s.close()
            # No UNLOCK in postgres, it unlocks automatically after transaction
            if (engine.execute(
                    "SELECT PG_CLASS.RELNAME, PG_LOCKS.MODE FROM PG_CLASS, PG_LOCKS WHERE PG_CLASS.OID = PG_LOCKS.RELATION AND PG_CLASS.RELNAMESPACE >= 2200;")):
                print("No lock for {}".format(engine.url))


