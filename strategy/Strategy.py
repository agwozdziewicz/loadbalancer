from abc import ABCMeta, abstractmethod

class Strategy(metaclass=ABCMeta):

    @abstractmethod
    def forwardRequest(self, ipDBTupleList):
        pass


class WeightRoundRoin(Strategy):

    position = 0

    def forwardRequest(self, ipDBTupleList):
        ipDBTupleList = ipDBTupleList.sort(key = ipDBTupleList[1]).reverse()
        with self.lock():
            if (self.position > len(ipDBTupleList)):
                position = 0
            targetDB = ipDBTupleList[position][0]
            position = position + 1
        return targetDB

class RoundRobin(Strategy):

    position = 0

    def forwardRequest(self, ipDBTupleList):
        with self.lock():
            if (self.position > len(ipDBTupleList)):
                position = 0
            targetDB = ipDBTupleList[position][0]
            position = position + 1
        return targetDB
