from typing import TypeVar

from config.entityManager import EntityManager
from repositories.abstractRepository import Repository

T = TypeVar('T')


#place to implement some custom, specific queries
class BookRepository(Repository[T]):
    tablename = "books"

    em: EntityManager

    def __init__(self, em: EntityManager, val: T) -> None:
        self.em = em
        Repository.__init__(self, em, val)

    def find_by_title(self, title):
        Repository.find_by_attr(self, self.val.title, title)

    def find_by_author(self, author):
        Repository.find_by_attr(self, self.val.author, author)

    def update_title(self, obj, new_title):
        Repository.update(self, obj, self.val.title, new_title)

    def update_author(self, obj, new_author):
        Repository.update(self, obj, self.val.author, new_author)
