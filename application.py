from config.entityManager import EntityManager
from entities.book import Book
from entities.user import User
from repositories.abstractRepository import Repository


if __name__ == '__main__':
    entityManager = EntityManager()
    bookRepository = Repository(entityManager)

    #userRepository = Repository[User](entityManager, User)
    #or custom book repository
    #bookRepository = BookRepository[Book](entityManager, Book)

    x = 0

    while x.__eq__(0):
        action = input(
            "\n\nWhat should I do for Book? [BA]dd, [BO]neById, [BL]ist, [BF]indBy, [BU]pdate, [BR]emove \n").upper()
            #"What should I do for User? [UA]dd, [UO]ne, [UL]ist \n").upper()

        #book sample
        if action == 'BA':
            book = Book()
            title = input("title: ")
            author = input("author: ")
            book.title = title
            book.author = author
            bookRepository.add(book)
        elif action == 'BO':
            obj_id = int(input("id: "))
            bookRepository.find_by_id(Book, obj_id)
        elif action == 'BL':
            bookRepository.find_all(Book)
        elif action == 'BF':
            action = input("How would you like to find the book: [A]uthor, [T]itle? ").upper()
            if action == "T":
                title = input("title: ")
                bookRepository.find_by_title(title)
            elif action == "A":
                author = input("author: ")
                bookRepository.find_by_author(author)
        elif action == "BU":
            obj_id = int(input("id: "))
            book = bookRepository.find_by_id(obj_id)
            action = input("What would you like to update: [T]itle, [A]uthor, [B]oth? ").upper()
        elif action == "BT":
            obj_id = int(input("id: "))
            book = bookRepository.update_by_id(Book, obj_id, "title", "test")
        elif action == "BR":
            obj_id = int(input("id: "))
            book = bookRepository.find_by_id(Book, obj_id)
            bookRepository.remove(Book, book)


















        #user sample
        elif action == 'UA':
            user = User()
            name = input("name: ")
            user.name = name
            userRepository.add(user)
        elif action == 'UO':
            obj_id = input("enter user id")
            userRepository.find_by_id(obj_id)
        elif action == 'UL':
            userRepository.find_all()

        # db operations
        elif action == 'C':
            print('creating tables')
            entityManager.create_all()
        elif action == 'D':
            print('dropping tables')
            entityManager.drop_all()
        elif action == 'X':
            x = 1
        else:
            print("I don't know how to do that")
            continue
