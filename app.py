import threading
import time
from client import test_sqlalchemy_orm
from config.entityManager import EntityManager
from lib.load_balancer import LoadBalancer

def main():
    entity_manager = EntityManager()
    LoadBalancer(entity_manager)
    entity_manager.drop_all()
    entity_manager.create_all()

    n = 10
    insert_percentage = 20
    update_percentage = 10
    delete_percentage = 10
    select_percentage = 30
    select_all_percentage = 30

    threads = list()
    for i in range(10):
        x = threading.Thread(target=test_sqlalchemy_orm,
                             args=(entity_manager, i, n, insert_percentage, update_percentage, delete_percentage,
                                   select_percentage, select_all_percentage))
        threads.append(x)
        x.start()

    for thread in threads:
        thread.join()


if __name__ == "__main__":
    start_time = time.time()
    main()
    print("--- %s seconds ---" % (time.time() - start_time))


